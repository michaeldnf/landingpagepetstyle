<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link href="https://fonts.googleapis.com/css2?family=Asap:wght@400;500;600;700&display=swap" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <link rel="stylesheet" href="css/style.css">

    <title>PetStyle - Todo o cuidado para o seu melhor amigo</title>
</head>
<body> 

  <?php if(isset($_GET['ok'])){ if($_GET['ok'] == 'ok'){?>
    <div class="modal-success">
      <div class="modal efeito">
        <div class="modal-close efeito" onclick="fecharModal()"><span>X</span></div>
        <div class="modal-text efeito">
          <h2>Muito obrigado!!!!</h2>
          <p>Foi enviado um e-mail com todas as informações necessárias.</p>
        </div>
      </div>
    </div>
  <?php }} ?>

  <header class="container topo">
    <img class="logo" src="images/LOGOTIPO PET.png" alt="logo tipo da empresa petStyle">
    
    <div class="frase-topo">
      <h2>A vida é mais <span>alegre</span> com um pet!
      <i class="fa fa-paw" aria-hidden="true"></i></h2>
    </div>  
  </header>
    
  <section class="background">
    <div class="container main">
      <div class="main-left">
          <div class="main-left-container">
            <div class="main-left-title">
              <h1>Seu <span class="destaque">pet</span> e nosso <span class="destaque">shop</span>, a
              <span class="destaque">combinação</span> perfeita para o cuidado com seus animais.</h1> 
            </div>

            <div class="main-left-text">
              <h2>Todo cuidado para seu melhor amigo sempre é pouco, que tal deixar ele 
              <span class="destaque">cheiroso</span> e <span class="destaque">comportado</span>.</h2>
            </div>
            <div class="main-left-promocao">
              <img src="images/promocao.png" alt="promoção da petshop">
            </div>
         </div>        
      </div>

      <div class="main-right">
        <form method="post" class="formulario" action="php/post.php">
          <img src="images/form-CAO.png" alt="">
          
          <h4>Cadastre-se agora e receba:<br>1 cupom promocional e<br>1 vídeo 
          <span class="destaque">AUAU</span>-la de treinamento!</h4>
          
          <?php if(isset($_GET['ok'])){ 
              if($_GET['ok'] == 'erro'){ ?> 
            <p>Preencha todos os campos!</p> 
          <?php }} ?>
          
          <input type="text" name="nome" placeholder="Digite o seu nome aqui..."/>
          <input type="email" name="email" placeholder="Digite seu e-mail aqui...">
          <input type="text" name="nomepet" placeholder="Digite o nome do seu pet aqui..."/>
          
          <div class="input-whatsapp">
            <i class="fa fa-whatsapp"  aria-hidden="true"></i> 
            <input type="text" name="numero" placeholder="Digite o seu WhatsApp aqui...">
          </div>
          
          <input type="submit" value="Enviar">   
        </form>
      </div>
    </div>
  </section>

  <script>
    function fecharModal(){
      document.getElementsByClassName('modal-success')[0]. style.animation = "fecharModal 0.8s";
      document.getElementsByClassName('modal-success')[0]. style.opacity = '0';
      document.getElementsByClassName('modal-success')[0]. style.display = 'none';
    }
  </script>
</body>
</html>